package main

import (
	"atlanta-api/login"
	"atlanta-api/usuario"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/hermescanuto/goconfigfromjsonfile"
)

var c = goconfigfromjsonfile.Getconfig()
var conn = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", c.User, c.Password, c.Host, c.Database)

func Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Not allowed")
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

type Carro struct{
	Ano int `json:"ano" `
	Cor string `json:"cor" `
	Nome string`json:"nome" "`
}

type Revenda struct{
	Carro []Carro
	roda string
}

func test(w http.ResponseWriter, r *http.Request) {
	var listas []Carro
	carA := Carro{1975,"Amarelo","Fusca"}
	listas =append(listas,carA)
    carB := Carro{1972,"Azul","Fusca"}
	listas =append(listas,carB)
    revenda := Revenda{Carro:listas,roda:"branca"}
	json.NewEncoder(w).Encode(revenda)
}

func main() {
	db, err := sql.Open("mysql", conn)
	if err != nil {
		panic("Erro ao abrir o banco de dados")
	}
	usuario.DB = db
	login.DB = db
	defer db.Close()

	router := mux.NewRouter()
	router.Use(commonMiddleware)

	router.HandleFunc("/", Handler)
	router.HandleFunc("/api_login/login", login.Login).Methods(http.MethodPost)
	router.HandleFunc("/api_usuario/usuario", usuario.GetAllUsuarioEndpoint).Methods(http.MethodGet)
	router.HandleFunc("/api_usuario/usuario/{id}", usuario.GetUsuarioEndpoint).Methods(http.MethodGet)

	router.HandleFunc("/teste", test).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe(c.Server, router))
}
