package util


type MsgErro struct {
	Status int `json:"status"`
}
const PROCESSAMENTO_OK = 1
const ERRO_SESSAO_EXPIRADA = 2
const ERRO_EMAIL_EXISTENTE = 3
const ERRO_LOGIN = 4
const ERRO_NAO_ENCONTRADO = 5

