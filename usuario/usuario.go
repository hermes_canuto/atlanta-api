package usuario

import (
	"atlanta-api/util"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)


type Usuario struct {
	Id_usuario      int    `json:"id_usuario"`
	Id_perfil       int    `json:"ip_perfil"`
	Id_tipo_usuario int    `json:"id_tipo_usuario"`
	Id_cargo        int    `json:"id_cargo"`
	Id_status       int    `json:"id_status"`
	Ch_nome         string `json:"ch_nome"`
	Ch_sobrenome    string `json:"ch_sobrenome"`
	Ch_apelido      string `json:"ch_apelido"`
	Ch_email        string `json:"ch_email"`
	Ch_senha        string `json:"ch_senha"`
	Ch_cpf          string `json:"ch_cpf"`
	Tp_camiseta     string `json:"ch_camiseta"`
	Dt_nascimento   string `json:"td_nascimento"`
	Dt_inclusao     string `json:"td_inclusao"`
	Dt_atualizacao  string `json:"td_atualizacao"`
	Dt_inativacao   string `json:"td_inativacao"`
	Ch_token        string `json:"ch_token"`
}

type Datas struct {
	Dt_nascimento  mysql.NullTime `json:"td_nascimento"`
	Dt_inclusao    mysql.NullTime `json:"td_inclusao"`
	Dt_atualizacao mysql.NullTime `json:"td_atualizacao"`
	Dt_inativacao  mysql.NullTime `json:"td_inativacao"`
}

type Retorno struct {
	*Usuario `json:"result"`
	Status   int `json:"status"`
}
type Retornobrowse struct {
	Usuarios *[]Usuario `json:"result"`
	Status   int       `json:"status"`
}

var DB *sql.DB

func checkErr1(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkErr(err error, p Usuario) interface{} {
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Not found")
			p.Id_usuario = -1
			return p
		} else {
			log.Fatal(err)
		}
	}
	return p
}

func GetUser(db *sql.DB, id int64) *Usuario {
	fmt.Println(id)
	var p Usuario
	var d Datas
	err := db.QueryRow("select * from tb_usuario where id_usuario=?", id).Scan(
		&p.Id_usuario,
		&p.Id_perfil,
		&p.Id_tipo_usuario,
		&p.Id_cargo,
		&p.Id_status,
		&p.Ch_nome,
		&p.Ch_sobrenome,
		&p.Ch_apelido,
		&p.Ch_email,
		&p.Ch_senha,
		&p.Ch_cpf,
		&p.Tp_camiseta,
		&d.Dt_nascimento,
		&d.Dt_inclusao,
		&d.Dt_atualizacao,
		&d.Dt_inativacao,
		&p.Ch_token)
	checkErr(err, p)
	p.Dt_nascimento = d.Dt_nascimento.Time.String()[0:19]
	p.Dt_inclusao = d.Dt_inclusao.Time.String()[0:19]
	p.Dt_atualizacao = d.Dt_atualizacao.Time.String()[0:19]
	p.Dt_inativacao = d.Dt_inativacao.Time.String()[0:19]
	return &p
}

func Browse(db *sql.DB) *[]Usuario {
	rows, err := db.Query("SELECT * FROM tb_usuario")
	checkErr1(err)
	var usuarios []Usuario
	var p Usuario
	var d Datas
	for rows.Next() {
		err = rows.Scan(
			&p.Id_usuario,
			&p.Id_perfil,
			&p.Id_tipo_usuario,
			&p.Id_cargo,
			&p.Id_status,
			&p.Ch_nome,
			&p.Ch_sobrenome,
			&p.Ch_apelido,
			&p.Ch_email,
			&p.Ch_senha,
			&p.Ch_cpf,
			&p.Tp_camiseta,
			&d.Dt_nascimento,
			&d.Dt_inclusao,
			&d.Dt_atualizacao,
			&d.Dt_inativacao,
			&p.Ch_token)
		checkErr1(err)
		p.Dt_nascimento = d.Dt_nascimento.Time.String()[0:19]
		p.Dt_inclusao = d.Dt_inclusao.Time.String()[0:19]
		p.Dt_atualizacao = d.Dt_atualizacao.Time.String()[0:19]
		p.Dt_inativacao = d.Dt_inativacao.Time.String()[0:19]
		usuarios = append(usuarios, p)
	}
	return &usuarios
}

func GetUsuarioEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	usuario := GetUser(DB, id)
	if usuario.Id_usuario == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(util.MsgErro{Status: util.ERRO_NAO_ENCONTRADO})
	} else {
		json.NewEncoder(w).Encode(Retorno{usuario, 1})
	}
}

func GetAllUsuarioEndpoint(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Retornobrowse{Browse(DB), 1})

}
