package login

import (
	"atlanta-api/util"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
)



type result struct {
	Retorno *retorno `json:"result"`
	Status  int      `json:"status"`
}

type retorno struct {
	Ch_token  string `json:"ch_token"`
	Id_perfil int    `json:"id_perfil"`
	Ch_nome   string `json:"ch_nome"`
	Ch_email  string `json:"ch_email"`
}

type entrada struct {
	Ch_email string
	Ch_senha string
}

var DB *sql.DB

func checkErr(err error, p retorno) interface{} {
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Not found")
			return false
		} else {
			log.Fatal(err)
		}
	}
	return p
}

func CheckUser(db *sql.DB, email string, senha string) retorno {
	var p retorno
	err := db.QueryRow("select ch_token,id_perfil,ch_nome,ch_email from tb_usuario where ch_email=? and ch_senha=md5(?)", email, senha).Scan(
		&p.Ch_token,
		&p.Id_perfil,
		&p.Ch_nome,
		&p.Ch_email)
	checkErr(err, p)
	return p
}

func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var l entrada
	_ = json.NewDecoder(r.Body).Decode(&l)
	usuario := CheckUser(DB, l.Ch_email, l.Ch_senha)
	if usuario.Id_perfil == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(util.MsgErro{ Status:util.ERRO_LOGIN})
	} else {
		fmt.Println(usuario)
		x := result{Retorno:&usuario,Status:util.PROCESSAMENTO_OK}
		json.NewEncoder(w).Encode(x)


	}

}
